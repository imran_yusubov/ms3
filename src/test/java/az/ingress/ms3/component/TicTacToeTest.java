package az.ingress.ms3.component;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

public class TicTacToeTest {

    private TicTacToe game;

    @BeforeEach
    void setUp() {
        game = new TicTacToe();
    }

    @Test
    void whenXOutsideBoardThenThrowRuntimeException() {
        //Arrange
        int x = 3;
        int y = 1;

        //Act & Assert
        assertThatThrownBy(() -> game.play(x, y))
                .isInstanceOf(RuntimeException.class)
                .hasMessage("X is outside board");
    }


    @Test
    void whenYOutsideBoardThenThrowRuntimeException() {
        //Arrange
        int x = 1;
        int y = -5;

        //Act & Assert
        assertThatThrownBy(() -> game.play(x, y))
                .isInstanceOf(RuntimeException.class)
                .hasMessage("Y is outside board");
    }

    @Test
    void whenOccupiedThenRuntimeException() {
        //Arrange
        int x = 2;
        int y = 2;

        //Act & Assert
        game.play(x, y);
        assertThatThrownBy(() -> game.play(x, y))
                .isInstanceOf(RuntimeException.class)
                .hasMessage("Box is occupied");
    }

    @Test
    void givenFirstTurnWhenNextPlayerThenX() {
        //Act & Assert
        assertThat(game.nextPlayer()).isEqualTo('X');
    }

    @Test
    void givenLastTurnWasXWhenNextPlayerThenO() {
        //Arrange
        int x = 2;
        int y = 2;

        //Act
        game.play(x, y);

        //Assert
        assertThat(game.nextPlayer()).isEqualTo('O');
    }

    @Test
    void whenPlayThenNoWinner() {
        //Arrange
        int x = 2;
        int y = 2;

        String actual = game.play(x, y);

        assertThat(actual).isEqualTo("Continued");
    }

    @Test
    void whenPlayAndWholeHorizontalLineThenWinner() {
        game.play(0, 0); //X
        game.play(1, 1); //O
        game.play(0, 1); //X
        game.play(1, 2); //O

        String actual = game.play(0, 2); //X

        assertThat(actual).isEqualTo("X is the winner");
    }

    @Test
    void whenPlayAndWholeVerticalLineThenWinner() {
        game.play(0, 0); //X
        game.play(1, 1); //O
        game.play(1, 0); //X
        game.play(1, 2); //O

        String actual = game.play(2, 0); //X

        assertThat(actual).isEqualTo("X is the winner");
    }

    @Test
    void whenPlayAndTopBottomDiagonalLineThenWinner(){
        game.play(0, 0); //X
        game.play(1, 0); //O
        game.play(1, 1); //X
        game.play(1, 2); //O

        String actual = game.play(2, 2); //X

        assertThat(actual).isEqualTo("X is the winner");
    }

    @Test
    void whenPlayAndBottomTopDiagonalLineThenWinner(){
        game.play(0, 2); //X
        game.play(1, 0); //O
        game.play(1, 1); //X
        game.play(1, 2); //O

        String actual = game.play(2, 0); //X

        assertThat(actual).isEqualTo("X is the winner");
    }

    @Test
    public void whenAllBoxesAreFilledThenDraw() {
        game.play(0, 0);
        game.play(0, 1);
        game.play(0, 2);
        game.play(1, 0);
        game.play(1, 2);
        game.play(1, 1);
        game.play(2, 0);
        game.play(2, 2);

        String actual = game.play(2, 1);

        assertThat(actual).isEqualTo("The result is draw");
    }
}
