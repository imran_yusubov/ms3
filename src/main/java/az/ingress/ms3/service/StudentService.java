package az.ingress.ms3.service;

import az.ingress.ms3.dto.StudentDto;
import az.ingress.ms3.exception.StudentAlreadyEixstException;
import az.ingress.ms3.exception.StudentNotFound;
import az.ingress.ms3.model.Student;
import az.ingress.ms3.repository.StudentRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class StudentService {

    private final StudentRepository studentRepository;
    private final ModelMapper mapper;

    public StudentDto createStudent(StudentDto studentDto) {
        log.info("Student details received {}", studentDto);
        studentRepository.findByEmail(studentDto.getEmail())
                .ifPresent((s) -> {
                    throw new StudentAlreadyEixstException(s.getEmail());
                });

        Student student = mapper.map(studentDto, Student.class);
        student.setId(null);
        final Student save = studentRepository.save(student);
        return mapper.map(save, StudentDto.class);
    }

    public StudentDto findById(Long id) {
        log.info("Get student by id {}", id);
        return studentRepository.findById(id)
                .map((s) -> mapper.map(s, StudentDto.class))
                .orElseThrow(() -> new StudentNotFound());
    }
}
