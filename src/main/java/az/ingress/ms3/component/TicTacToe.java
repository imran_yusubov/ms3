package az.ingress.ms3.component;

public class TicTacToe {

    private static final int SIZE = 3;

    private final char[][] board = new char[][]{
            {'-', '-', '-'},
            {'-', '-', '-'},
            {'-', '-', '-'}
    };
    private char lastPlayer;

    public String play(int x, int y) {
        checkCoordinate(x, y);
        lastPlayer = nextPlayer();
        setBox(x, y, lastPlayer);

        if (isWin(x, y)) {
            return lastPlayer + " is the winner";
        } else if (isDraw()) {
            return "The result is draw";
        } else {
            return "Continued";
        }
    }

    private boolean isDraw() {
        for (int i = 0; i < SIZE; i++) {
            for (int j = 0; j < SIZE; j++) {
                if (board[i][j] == '-') {
                    return false;
                }
            }
        }
        return true;
    }

    private void setBox(int x, int y, char lastPlayer) {
        if (board[x][y] != '-') {
            throw new RuntimeException("Box is occupied");
        } else {
            board[x][y] = lastPlayer;
        }
    }

    private void checkCoordinate(int x, int y) {
        if (x < 0 || x > 2) {
            throw new RuntimeException("X is outside board");
        }
        if (y < 0 || y > 2) {
            throw new RuntimeException("Y is outside board");
        }
    }

    public char nextPlayer() {
        if (lastPlayer == 'X') {
            return 'O';
        }
        return 'X';
    }

    private boolean isWin(int x, int y) {
        int playerTotal = lastPlayer * 3;
        char horizontal, vertical, diagonal1, diagonal2;
        horizontal = vertical = diagonal1 = diagonal2 = '\0';
        for (int i = 0; i < SIZE; i++) {
            horizontal += board[i][y];
            vertical += board[x][i];
            diagonal1 += board[i][i];
            diagonal2 += board[i][SIZE - i - 1];
        }
        return horizontal == playerTotal
                || vertical == playerTotal
                || diagonal1 == playerTotal
                || diagonal2 == playerTotal;
    }


}
